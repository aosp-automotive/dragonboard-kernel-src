// SPDX-License-Identifier: GPL-2.0
// Copyright (c) 2018, Linaro Limited

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_device.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/pcm.h>
#include "common.h"
#include "qdsp6/q6afe.h"

#define SLIM_MAX_TX_PORTS 16
#define SLIM_MAX_RX_PORTS 16
#define WCD9335_DEFAULT_MCLK_RATE	9600000
#define MI2S_BCLK_RATE			1536000
#define PCM_BCLK_RATE			1024000

static int apq8096_be_hw_params_fixup(struct snd_soc_pcm_runtime *rtd,
				      struct snd_pcm_hw_params *params)
{
	struct snd_interval *rate = hw_param_interval(params,
					SNDRV_PCM_HW_PARAM_RATE);
	struct snd_interval *channels = hw_param_interval(params,
					SNDRV_PCM_HW_PARAM_CHANNELS);

	rate->min = rate->max = 48000;
	channels->min = channels->max = 2;

	return 0;
}

static int pri_pcm_sample_rate = 16000;
static int quat_pcm_sample_rate = 16000;

static int msm_snd_hw_params(struct snd_pcm_substream *substream,
			     struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_interval *rate = hw_param_interval(params,
					SNDRV_PCM_HW_PARAM_RATE);
	struct snd_interval *channels = hw_param_interval(params,
					SNDRV_PCM_HW_PARAM_CHANNELS);
	u32 rx_ch[SLIM_MAX_RX_PORTS], tx_ch[SLIM_MAX_TX_PORTS];
	u32 rx_ch_cnt = 0, tx_ch_cnt = 0;
	int ret = 0;

	switch (cpu_dai->id) {
	case PRIMARY_PCM_RX:
	case PRIMARY_PCM_TX:
		rate->min = rate->max = pri_pcm_sample_rate;
		channels->min = channels->max = 1;
		break;
	case QUATERNARY_PCM_RX:
	case QUATERNARY_PCM_TX:
		rate->min = rate->max = quat_pcm_sample_rate;
		channels->min = channels->max = 1;
		break;
	default:
		rate->min = 48000;
		rate->max = 48000;
		channels->min = 2;
		channels->max = 2;
		break;
	}

	ret = snd_soc_dai_get_channel_map(codec_dai,
				&tx_ch_cnt, tx_ch, &rx_ch_cnt, rx_ch);
	if (ret != 0 && ret != -ENOTSUPP) {
		pr_err("failed to get codec chan map, err:%d\n", ret);
		goto end;
	} else if (ret == -ENOTSUPP) {
		return 0;
	}

	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
		ret = snd_soc_dai_set_channel_map(cpu_dai, 0, NULL,
						  rx_ch_cnt, rx_ch);
	else
		ret = snd_soc_dai_set_channel_map(cpu_dai, tx_ch_cnt, tx_ch,
						  0, NULL);
	if (ret != 0 && ret != -ENOTSUPP)
		pr_err("Failed to set cpu chan map, err:%d\n", ret);
	else if (ret == -ENOTSUPP)
		ret = 0;
end:
	return ret;
}

static int msm_snd_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;

	switch (cpu_dai->id) {
	case PRIMARY_MI2S_RX:
	case PRIMARY_MI2S_TX:
		snd_soc_dai_set_sysclk(cpu_dai,
			Q6AFE_LPASS_CLK_ID_PRI_MI2S_IBIT,
			MI2S_BCLK_RATE, SNDRV_PCM_STREAM_PLAYBACK);
		snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_CBS_CFS);
		snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_CBS_CFS);
		break;
	case QUATERNARY_MI2S_RX:
	case QUATERNARY_MI2S_TX:
		snd_soc_dai_set_sysclk(cpu_dai,
			Q6AFE_LPASS_CLK_ID_QUAD_MI2S_IBIT,
			MI2S_BCLK_RATE, SNDRV_PCM_STREAM_PLAYBACK);
		snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_CBS_CFS);
		snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_CBS_CFS);
		break;
	case PRIMARY_PCM_RX:
	case PRIMARY_PCM_TX:
		snd_soc_dai_set_sysclk(cpu_dai,
			Q6AFE_LPASS_CLK_ID_PRI_PCM_IBIT,
			PCM_BCLK_RATE, SNDRV_PCM_STREAM_PLAYBACK);
		snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_CBS_CFS);
		snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_CBS_CFS);
		break;
	case QUATERNARY_PCM_RX:
	case QUATERNARY_PCM_TX:
		snd_soc_dai_set_sysclk(cpu_dai,
			Q6AFE_LPASS_CLK_ID_QUAD_PCM_IBIT,
			PCM_BCLK_RATE, SNDRV_PCM_STREAM_PLAYBACK);
		snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_CBS_CFS);
		snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_CBS_CFS);
		break;
	default:
		return -1;
	}
	return 0;
}

static struct snd_soc_ops apq8096_ops = {
	.hw_params = msm_snd_hw_params,
	.startup = msm_snd_startup,
};

static char const *pcm_sample_rate_text[] = {"8 kHz", "16 kHz"};
static const struct soc_enum pcm_snd_enum = SOC_ENUM_SINGLE_EXT(2, pcm_sample_rate_text);

static int get_sample_rate_idx(int sample_rate)
{
	int sample_rate_idx = 0;

	switch (sample_rate) {
	case 8000:
		sample_rate_idx = 0;
		break;
	case 16000:
	default:
		sample_rate_idx = 1;
		break;
	}

	return sample_rate_idx;
}

static int pri_pcm_sample_rate_get(struct snd_kcontrol *kcontrol,
        struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = get_sample_rate_idx(pri_pcm_sample_rate);
	return 0;
}

static int quat_pcm_sample_rate_get(struct snd_kcontrol *kcontrol,
        struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = get_sample_rate_idx(quat_pcm_sample_rate);
	return 0;
}

static int get_sample_rate(int idx)
{
	int sample_rate_val = 0;

	switch (idx) {
	case 0:
		sample_rate_val = 8000;
		break;
	case 1:
	default:
		sample_rate_val = 16000;
		break;
	}

	return sample_rate_val;
}

static int pri_pcm_sample_rate_put(struct snd_kcontrol *kcontrol,
        struct snd_ctl_elem_value *ucontrol)
{
	pri_pcm_sample_rate = get_sample_rate(ucontrol->value.integer.value[0]);
	return 0;
}

static int quat_pcm_sample_rate_put(struct snd_kcontrol *kcontrol,
        struct snd_ctl_elem_value *ucontrol)
{
	quat_pcm_sample_rate = get_sample_rate(ucontrol->value.integer.value[0]);
	return 0;
}

static const struct snd_kcontrol_new card_controls[] = {
	SOC_ENUM_EXT("PRI_PCM SampleRate", pcm_snd_enum,
			pri_pcm_sample_rate_get, pri_pcm_sample_rate_put),
	SOC_ENUM_EXT("QUAT_PCM SampleRate", pcm_snd_enum,
			quat_pcm_sample_rate_get, quat_pcm_sample_rate_put),
};

static int apq8096_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_dai *codec_dai = rtd->codec_dai;

	/*
	 * Codec SLIMBUS configuration
	 * RX1, RX2, RX3, RX4, RX5, RX6, RX7, RX8, RX9, RX10, RX11, RX12, RX13
	 * TX1, TX2, TX3, TX4, TX5, TX6, TX7, TX8, TX9, TX10, TX11, TX12, TX13
	 * TX14, TX15, TX16
	 */
	unsigned int rx_ch[SLIM_MAX_RX_PORTS] = {144, 145, 146, 147, 148, 149,
					150, 151, 152, 153, 154, 155, 156};
	unsigned int tx_ch[SLIM_MAX_TX_PORTS] = {128, 129, 130, 131, 132, 133,
					    134, 135, 136, 137, 138, 139,
					    140, 141, 142, 143};

	snd_soc_dai_set_channel_map(codec_dai, ARRAY_SIZE(tx_ch),
					tx_ch, ARRAY_SIZE(rx_ch), rx_ch);

	snd_soc_dai_set_sysclk(codec_dai, 0, WCD9335_DEFAULT_MCLK_RATE,
				SNDRV_PCM_STREAM_PLAYBACK);

	return 0;
}

static void apq8096_add_be_ops(struct snd_soc_card *card)
{
	struct snd_soc_dai_link *link;
	int i;

	for_each_card_prelinks(card, i, link) {
		if (link->no_pcm == 1) {
			link->init = apq8096_init;
			link->ops = &apq8096_ops;
		}
	}
}

static struct snd_soc_codec_conf codec_conf[] = {
	{
		.dev_name = "pcm512x.1-004c",
		.name_prefix = "DAC-F",
	},
	{
		.dev_name = "pcm512x.1-004d",
		.name_prefix = "DAC-R",
	},
};

static int apq8096_platform_probe(struct platform_device *pdev)
{
	struct snd_soc_card *card;
	struct device *dev = &pdev->dev;
	int ret;

	card = kzalloc(sizeof(*card), GFP_KERNEL);
	if (!card)
		return -ENOMEM;

	card->dev = dev;
	dev_set_drvdata(dev, card);
	ret = qcom_snd_parse_of(card);
	if (ret) {
		dev_err(dev, "Error parsing OF data\n");
		goto err;
	}

	apq8096_add_be_ops(card);
	card->codec_conf = codec_conf;
	card->num_configs = ARRAY_SIZE(codec_conf);
	ret = snd_soc_register_card(card);
	if (ret)
		goto err_card_register;

        snd_soc_add_card_controls(card, card_controls,
                                        ARRAY_SIZE(card_controls));

	return 0;

err_card_register:
	kfree(card->dai_link);
err:
	kfree(card);
	return ret;
}

static int apq8096_platform_remove(struct platform_device *pdev)
{
	struct snd_soc_card *card = dev_get_drvdata(&pdev->dev);

	snd_soc_unregister_card(card);
	kfree(card->dai_link);
	kfree(card);

	return 0;
}

static const struct of_device_id msm_snd_apq8096_dt_match[] = {
	{.compatible = "qcom,apq8096-sndcard"},
	{}
};

MODULE_DEVICE_TABLE(of, msm_snd_apq8096_dt_match);

static struct platform_driver msm_snd_apq8096_driver = {
	.probe  = apq8096_platform_probe,
	.remove = apq8096_platform_remove,
	.driver = {
		.name = "msm-snd-apq8096",
		.of_match_table = msm_snd_apq8096_dt_match,
	},
};
module_platform_driver(msm_snd_apq8096_driver);
MODULE_AUTHOR("Srinivas Kandagatla <srinivas.kandagatla@linaro.org");
MODULE_DESCRIPTION("APQ8096 ASoC Machine Driver");
MODULE_LICENSE("GPL v2");
