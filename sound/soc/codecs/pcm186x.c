// SPDX-License-Identifier: GPL-2.0
/*
 * Texas Instruments PCM186x Universal Audio ADC
 *
 * Copyright (C) 2015-2017 Texas Instruments Incorporated - http://www.ti.com
 *	Andreas Dannenberg <dannenberg@ti.com>
 *	Andrew F. Davis <afd@ti.com>
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>
#include <linux/regulator/consumer.h>
#include <linux/regmap.h>
#include <linux/slab.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/jack.h>
#include <sound/initval.h>
#include <sound/tlv.h>

#include "pcm186x.h"

static const char * const pcm186x_supply_names[] = {
	"avdd",		/* Analog power supply. Connect to 3.3-V supply. */
	"dvdd",		/* Digital power supply. Connect to 3.3-V supply. */
	"iovdd",	/* I/O power supply. Connect to 3.3-V or 1.8-V. */
};
#define PCM186x_NUM_SUPPLIES ARRAY_SIZE(pcm186x_supply_names)

struct pcm186x_priv {
	struct regmap *regmap;
	struct regulator_bulk_data supplies[PCM186x_NUM_SUPPLIES];
	unsigned int sysclk;
	unsigned int tdm_offset;
	bool is_tdm_mode;
	bool is_master_mode;
};

static const DECLARE_TLV_DB_SCALE(pcm186x_pga_tlv, -1200, 50, 0);

struct dsp_code {
	char i2c_addr;
	char offset;
	char val;
};

struct glb_pool {
	struct mutex lock;
	unsigned int dsp_page_number;
};

/*
 * The PCM186x DSP register map has holes in it, hence we define a table so we
 * can process all locations elements in an iterative manner.
 */
static const u8 pcm186x_dsp_registers[] = {
	PCM186X_DSP_MIX1_CH1L,
	PCM186X_DSP_MIX1_CH1R,
	PCM186X_DSP_MIX1_CH2L,
	PCM186X_DSP_MIX1_CH2R,
	PCM186X_DSP_MIX1_I2SL,
	PCM186X_DSP_MIX1_I2SR,
	PCM186X_DSP_MIX2_CH1L,
	PCM186X_DSP_MIX2_CH1R,
	PCM186X_DSP_MIX2_CH2L,
	PCM186X_DSP_MIX2_CH2R,
	PCM186X_DSP_MIX2_I2SL,
	PCM186X_DSP_MIX2_I2SR,
	PCM186X_DSP_MIX3_CH1L,
	PCM186X_DSP_MIX3_CH1R,
	PCM186X_DSP_MIX3_CH2L,
	PCM186X_DSP_MIX3_CH2R,
	PCM186X_DSP_MIX3_I2SL,
	PCM186X_DSP_MIX3_I2SR,
	PCM186X_DSP_MIX4_CH1L,
	PCM186X_DSP_MIX4_CH1R,
	PCM186X_DSP_MIX4_CH2L,
	PCM186X_DSP_MIX4_CH2R,
	PCM186X_DSP_MIX4_I2SL,
	PCM186X_DSP_MIX4_I2SR,
	PCM186X_DSP_LPF_B0,
	PCM186X_DSP_LPF_B1,
	PCM186X_DSP_LPF_B2,
	PCM186X_DSP_LPF_A1,
	PCM186X_DSP_LPF_A2,
	PCM186X_DSP_HPF_B0,
	PCM186X_DSP_HPF_B1,
	PCM186X_DSP_HPF_B2,
	PCM186X_DSP_HPF_A1,
	PCM186X_DSP_HPF_A2,
	PCM186X_DSP_LOSS_THRESH,
	PCM186X_DSP_RES_THRESH,
};

static const u8 pcm1865_dsp_vals[] = {
	/* MIX1 = I2S-OUT1-Left to DAC's
	 * MIX2 = I2S-OUT1-Right to DAC's
	 * MIX3 = I2S-OUT2-Left to SBC
	 * MIX4 = I2S-OUT2-Right to SBC
	 * CH1 and CH2 are outputs of ADC1 and ADC2
	 *
	 * ADC1 mixed to I2S-OUT1
	 * I2S-IN mixed to I2S-OUT1
	 *
	 * ADC2 to I2S-OUT2 (to SBC)
	 */ 

	/*PCM186X_DSP_MIX1_CH1L =*/ 1,
	/*PCM186X_DSP_MIX1_CH1R =*/ 0,
	/*PCM186X_DSP_MIX1_CH2L =*/ 0,
	/*PCM186X_DSP_MIX1_CH2R =*/ 0,
	/*PCM186X_DSP_MIX1_I2SL =*/ 1,
	/*PCM186X_DSP_MIX1_I2SR =*/ 0,

	/*PCM186X_DSP_MIX2_CH1L =*/ 0,
	/*PCM186X_DSP_MIX2_CH1R =*/ 1,
	/*PCM186X_DSP_MIX2_CH2L =*/ 0,
	/*PCM186X_DSP_MIX2_CH2R =*/ 0,
	/*PCM186X_DSP_MIX2_I2SL =*/ 0,
	/*PCM186X_DSP_MIX2_I2SR =*/ 1,

	/*PCM186X_DSP_MIX3_CH1L =*/ 0,
	/*PCM186X_DSP_MIX3_CH1R =*/ 0,
	/*PCM186X_DSP_MIX3_CH2L =*/ 1,
	/*PCM186X_DSP_MIX3_CH2R =*/ 0,
	/*PCM186X_DSP_MIX3_I2SL =*/ 0,
	/*PCM186X_DSP_MIX3_I2SR =*/ 0,

	/*PCM186X_DSP_MIX4_CH1L =*/ 0,
	/*PCM186X_DSP_MIX4_CH1R =*/ 0,
	/*PCM186X_DSP_MIX4_CH2L =*/ 0,
	/*PCM186X_DSP_MIX4_CH2R =*/ 1,
	/*PCM186X_DSP_MIX4_I2SL =*/ 0,
	/*PCM186X_DSP_MIX4_I2SR =*/ 0,
};

int set_page_1(struct snd_soc_component *component){
	int i, ret, val;

	for (i = 0; i < PCM186X_MAX_NR_BUSY_CHECKS; i++) {
		ret = snd_soc_component_write(component, PCM186X_PAGE, PCM186X_PAGE_1);
		if (ret < 0) {
			dev_err(component->dev, "Error writing device: %d\n", ret);
			return ret;
		}

		// Writing the page register at least twice before start
		// checking the MMAP status register for it to become zero.
		if (i < 1)
			continue;

		ret = snd_soc_component_read(component, PCM186X_MMAP_STAT_CTRL, &val);
		if (ret < 0){
			 // One reason for encountering a timeout error can be that the PCM185x
			 // DSPs are not being clocked because the driving clock source has been
			 // turned off which can happen in a number of different operating
			 // scenarios.
			dev_err(component->dev, "Timeout accessing DSP memory\n");
			return val;
		}
		if (val == 0)
			break;
	}
	return 0;
}

uint32_t read_coefficient(struct snd_soc_component *component, int reg){
	int j, val, rval, ret;

	// Set virtual address
	ret = snd_soc_component_write(component, PCM186X_MMAP_ADDRESS, reg);
	if (ret < 0) {
		dev_err(component->dev, "Error writing device: %d\n", ret);
		return ret;
	}

	/* Issue read request */
	ret = snd_soc_component_write(component, PCM186X_MMAP_STAT_CTRL,
			   PCM186X_MMAP_STAT_R_REQ);
	if (ret < 0) {
		dev_err(component->dev, "Error writing device: %d\n", ret);
		return ret;
	}

	/* Wait for the DSP register read to finish but not forever */
	for (j = 0; j < PCM186X_MAX_NR_BUSY_CHECKS; j++) {
		ret = snd_soc_component_read(component, PCM186X_MMAP_STAT_CTRL, &val);
		if (ret < 0) {
			dev_err(component->dev, "Error reading device\n");
			return -EBUSY;
		}
		if (val == 0)
			break;
	}

	if (j == PCM186X_MAX_NR_BUSY_CHECKS) {
		dev_err(component->dev, "Timeout reading DSP coefficients\n");
		return -EBUSY;
	}

	/* Get, assemble, and populate return data - 24 bits each */
	ret = snd_soc_component_read(component, PCM186X_MEM_RDATA0, &val);
	if (ret < 0) {
		dev_err(component->dev, "Error reading device\n");
		return -EBUSY;
	}
	rval = ((val << 16) & 0xff0000);

	ret = snd_soc_component_read(component, PCM186X_MEM_RDATA1, &val);
	if (ret < 0) {
		dev_err(component->dev, "Error reading device\n");
		return -EBUSY;
	}
	rval |= ((val << 8) & 0xff00);

	ret = snd_soc_component_read(component, PCM186X_MEM_RDATA2, &val);
	if (ret < 0) {
		dev_err(component->dev, "Error reading device\n");
		return -EBUSY;
	}
	rval |= (val & 0xff);

	return rval;
}

int write_coefficient(struct snd_soc_component *component, int reg, int wdata0, int wdata1, int wdata2){
	int j, ret, val;

	// Set virtual address
	ret = snd_soc_component_write(component, PCM186X_MMAP_ADDRESS, reg);
	if (ret < 0) {
		dev_err(component->dev, "Error writing device: %d\n", ret);
		return ret;
	}

	// Set data to write - 24 bits each
	// Switch mixer inputs on/off based on static array.
	// 0x100000 = ON, 0x000000 = OFF
	ret = snd_soc_component_write(component, PCM186X_MEM_WDATA0, wdata0);
	if (ret < 0) {
		dev_err(component->dev, "Error writing device: %d\n", ret);
		return ret;
	}

	ret = snd_soc_component_write(component, PCM186X_MEM_WDATA1, wdata1);
	if (ret < 0) {
		dev_err(component->dev, "Error writing device: %d\n", ret);
		return ret;
	}

	ret = snd_soc_component_write(component, PCM186X_MEM_WDATA2, wdata2);
	if (ret < 0) {
		dev_err(component->dev, "Error writing device: %d\n", ret);
		return ret;
	}

	 // Issue write request. Note that DSP Internal memory can only
	 // be written to when the DSP is running, requesting a WREQ=1
	 // will have no effect otherwise. This is of relevance if the
	 // codec is running as a clock slave, and the clocks stop.
	ret = snd_soc_component_write(component, PCM186X_MMAP_STAT_CTRL, PCM186X_MMAP_STAT_W_REQ);
	if (ret < 0) {
		dev_err(component->dev, "Error to write device: %d\n", ret);
		return ret;
	}

	// Wait for the DSP register write to finish but not forever
	for (j = 0; j < PCM186X_MAX_NR_BUSY_CHECKS; j++) {
		ret = snd_soc_component_read(component, PCM186X_MMAP_STAT_CTRL, &val);
		if (ret < 0) {
			dev_err(component->dev, "Error reading device: %d\n", ret);
			return ret;
		}
		if (!val)
			break;
	}

	if (j == PCM186X_MAX_NR_BUSY_CHECKS) {
		dev_err(component->dev, "Timeout writing DSP coefficients\n");
		return -EBUSY;
	}

	return 0;
}

/* This is not magic. This is just a lookup table with values generated using floating point math that is bad for kernels.
 * Coefficient = (10 ^ (dB / 20)) * (2 ^ 20) where dB is [-120.0, 18.0] with 0.25 dB steps
 * The coefficient is a 24 bit number, with each of its 3 bytes split amongst 3 arrays:
 *   for (i = 0, f = -120.0; f <= 18.0; f += 0.25, i++){
 *     coefficient = (int)(pow(10.0, (f / 20.0)) * 1048576.0);
 *     pcm1865_dsp_coefficients[0][i] = coefficient & 0xff0000 >> 16;
 *     pcm1865_dsp_coefficients[1][i] = coefficient & 0xff00 >> 8;
 *     pcm1865_dsp_coefficients[2][i] = coefficient & 0xff;
 *   }
 *
 * 0 dB is at position 480 in the arrays [0x10, 0x00, 0x00].
 * For 50% volume use 480*50% = 240;
 * DATA0 = pcm1865_dsp_coefficients[0][240];
 * DATA1 = pcm1865_dsp_coefficients[1][240];
 * DATA2 = pcm1865_dsp_coefficients[2][240];
 */
static const uint8_t pcm1865_dsp_coefficients[3][553] = {
/*MSB*/	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x05,0x05,0x05,0x05,0x05,0x05,0x06,0x06,0x06,0x06,0x06,0x06,0x07,0x07,0x07,0x07,0x08,0x08,0x08,0x08,0x08,0x09,0x09,0x09,0x0A,0x0A,0x0A,0x0B,0x0B,0x0B,0x0B,0x0C,0x0C,0x0D,0x0D,0x0D,0x0E,0x0E,0x0F,0x0F,0x10,0x10,0x10,0x11,0x11,0x12,0x13,0x13,0x14,0x14,0x15,0x15,0x16,0x17,0x17,0x18,0x19,0x1A,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x27,0x28,0x29,0x2A,0x2B,0x2D,0x2E,0x2F,0x31,0x32,0x34,0x35,0x37,0x38,0x3A,0x3C,0x3D,0x3F,0x41,0x43,0x45,0x47,0x49,0x4B,0x4D,0x50,0x52,0x54,0x57,0x59,0x5C,0x5F,0x62,0x64,0x67,0x6A,0x6E,0x71,0x74,0x77,0x7B,0x7F},
	{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x05,0x05,0x05,0x05,0x05,0x05,0x05,0x06,0x06,0x06,0x06,0x06,0x07,0x07,0x07,0x07,0x07,0x08,0x08,0x08,0x08,0x09,0x09,0x09,0x09,0x0A,0x0A,0x0A,0x0B,0x0B,0x0B,0x0C,0x0C,0x0C,0x0D,0x0D,0x0E,0x0E,0x0E,0x0F,0x0F,0x10,0x10,0x11,0x11,0x12,0x12,0x13,0x13,0x14,0x15,0x15,0x16,0x17,0x17,0x18,0x19,0x19,0x1A,0x1B,0x1C,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x2A,0x2B,0x2C,0x2D,0x2F,0x30,0x32,0x33,0x35,0x36,0x38,0x39,0x3B,0x3D,0x3F,0x40,0x42,0x44,0x46,0x48,0x4A,0x4D,0x4F,0x51,0x54,0x56,0x59,0x5B,0x5E,0x61,0x63,0x66,0x69,0x6C,0x70,0x73,0x76,0x7A,0x7D,0x81,0x85,0x89,0x8D,0x91,0x95,0x99,0x9E,0xA3,0xA7,0xAC,0xB1,0xB6,0xBC,0xC1,0xC7,0xCD,0xD3,0xD9,0xDF,0xE6,0xED,0xF3,0xFB,0x02,0x09,0x11,0x19,0x21,0x2A,0x33,0x3C,0x45,0x4E,0x58,0x62,0x6D,0x77,0x82,0x8D,0x99,0xA5,0xB1,0xBE,0xCB,0xD8,0xE6,0xF5,0x03,0x12,0x22,0x32,0x42,0x53,0x64,0x76,0x89,0x9C,0xAF,0xC3,0xD8,0xED,0x03,0x1A,0x31,0x49,0x61,0x7A,0x94,0xAF,0xCB,0xE7,0x04,0x22,0x41,0x61,0x82,0xA4,0xC6,0xEA,0x0F,0x35,0x5C,0x84,0xAD,0xD7,0x03,0x30,0x5E,0x8E,0xBF,0xF1,0x25,0x5B,0x92,0xCA,0x04,0x40,0x7E,0xBD,0xFF,0x42,0x87,0xCF,0x18,0x63,0xB1,0x01,0x53,0xA8,0xFF,0x59,0xB5,0x14,0x76,0xDA,0x42,0xAD,0x1A,0x8B,0x00,0x77,0xF2,0x71,0xF3,0x79,0x04,0x92,0x24,0xBB,0x56,0xF5,0x99,0x42,0xF0,0xA3,0x5B,0x19,0xDC,0xA5,0x73,0x48,0x23,0x04,0xEC,0xDB,0xD0,0xCD,0xD1,0xDD,0xF1,0x0C,0x30,0x5D,0x92,0xD0,0x18,0x69,0xC4,0x29,0x98,0x12,0x98,0x28,0xC5,0x6D,0x22,0xE3,0xB2,0x8E,0x78,0x71,0x78,0x8E,0xB4,0xEA,0x30,0x88,0xF1,0x6B,0xF9,0x9A,0x4E,0x16,0xF4,0xE6,0xEF,0x0E,0x45,0x94,0xFB,0x7C,0x17},
/*LSB*/	{0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x05,0x05,0x05,0x05,0x05,0x05,0x06,0x06,0x06,0x06,0x06,0x07,0x07,0x07,0x07,0x07,0x08,0x08,0x08,0x08,0x09,0x09,0x09,0x09,0x0A,0x0A,0x0A,0x0B,0x0B,0x0B,0x0C,0x0C,0x0C,0x0D,0x0D,0x0D,0x0E,0x0E,0x0F,0x0F,0x10,0x10,0x11,0x11,0x12,0x12,0x13,0x13,0x14,0x14,0x15,0x16,0x16,0x17,0x18,0x18,0x19,0x1A,0x1B,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2C,0x2D,0x2E,0x30,0x31,0x33,0x34,0x36,0x37,0x39,0x3A,0x3C,0x3E,0x40,0x42,0x44,0x46,0x48,0x4A,0x4C,0x4E,0x50,0x53,0x55,0x58,0x5A,0x5D,0x60,0x62,0x65,0x68,0x6B,0x6F,0x72,0x75,0x79,0x7C,0x80,0x84,0x87,0x8B,0x8F,0x94,0x98,0x9C,0xA1,0xA6,0xAB,0xB0,0xB5,0xBA,0xBF,0xC5,0xCB,0xD1,0xD7,0xDD,0xE4,0xEA,0xF1,0xF8,0xFF,0x07,0x0F,0x16,0x1F,0x27,0x30,0x39,0x42,0x4B,0x55,0x5F,0x69,0x74,0x7E,0x8A,0x95,0xA1,0xAD,0xBA,0xC7,0xD4,0xE2,0xF0,0xFE,0x0D,0x1C,0x2C,0x3C,0x4D,0x5E,0x70,0x82,0x95,0xA8,0xBC,0xD1,0xE6,0xFC,0x12,0x29,0x40,0x59,0x72,0x8C,0xA6,0xC1,0xDD,0xFA,0x18,0x37,0x56,0x77,0x98,0xBA,0xDE,0x02,0x28,0x4E,0x76,0x9F,0xC9,0xF4,0x20,0x4E,0x7D,0xAE,0xE0,0x13,0x48,0x7F,0xB7,0xF0,0x2C,0x69,0xA8,0xE8,0x2B,0x70,0xB6,0xFF,0x49,0x96,0xE5,0x37,0x8B,0xE1,0x3A,0x95,0xF3,0x54,0xB8,0x1E,0x88,0xF5,0x64,0xD8,0x4E,0xC8,0x45,0xC6,0x4B,0xD4,0x61,0xF2,0x87,0x20,0xBE,0x61,0x08,0xB4,0x65,0x1C,0xD8,0x99,0x60,0x2C,0xFF,0xD8,0xB7,0x9C,0x89,0x7C,0x76,0x78,0x81,0x92,0xAB,0xCC,0xF5,0x27,0x63,0xA7,0xF5,0x4C,0xAE,0x1A,0x90,0x12,0x9E,0x37,0xDB,0x8C,0x49,0x13,0xEA,0xD0,0xC3,0xC5,0xD6,0xF7,0x27,0x68,0xB9,0x1C,0x91,0x18,0xB2,0x60,0x21,0xF7,0xE3,0xE4,0xFB,0x2A,0x70,0xCF,0x48,0xDA,0x86,0x4F,0x33,0x35,0x54,0x93,0xF1,0x70,0x10,0xD3,0xBA,0xC5,0xF6,0x4D,0xCD,0x76,0x49,0x47,0x73,0xCC,0x55,0x0F,0xFB,0x1B,0x70,0xFC,0xC0,0xBF,0xF9,0x71,0x28,0x20,0x5B,0xDB,0xA2,0xB2,0x0E,0xB7,0xAF,0xFA,0x99,0x8F,0xDE,0x8A,0x94,0xFF,0xCF,0x06,0xA7,0xB6,0x35,0x28,0x93,0x78,0xDB,0xC0,0x2C,0x20,0xA3,0xB7,0x62,0xA7,0x8A,0x12,0x42,0x1F,0xAF,0xF6,0xFA,0xC1,0x50,0xAD,0xDE,0xE9,0xD5,0xA8,0x68,0x1E,0xD0,0x84,0x44,0x17,0x04,0x15,0x50,0xC0,0x6E,0x61,0xA5,0x43,0x44,0xB4,0x9D,0x0A,0x07,0x9E,0xDC,0xCE,0x80,0xFF,0x59,0x9B,0xD5,0x13,0x66,0xDD,0x89,0x79,0xBE,0x6B,0x91,0x42,0x91,0x93,0x5A,0xFD,0x90,0x29,0xDF,0xC9,0x00,0x9A,0xB4,0x65,0xC9,0xFC,0x1A,0x41,0x8E,0x21,0x1A,0x99,0xC0,0xB3,0x94,0x89,0xB8,0x48,0x61,0x2C,0xD5,0x86,0x6D,0xB8,0x98,0x3D,0xD9,0xA2,0xCD,0x90,0x25,0xC6,0xAF,0x1F,0x54,0x91,0x18,0x30,0x20,0x31,0xB0,0xEA,0x2F,0xD2,0x28,0x88,0x4C,0xD2,0x78,0xA0,0xB0,0x0F,0x28,0x6A,0x46,0x30,0xA1,0x13,0x06,0xFE,0x80,0x17,0x52,0xC3,0x03,0xAB,0x5D,0xBD,0x75,0x33,0xAA,0x94,0xAF}
};

static int pcm1865_get_capture_mute(struct snd_kcontrol *kcontrol,
			      struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);
	int val;
	snd_soc_component_read(component, PCM186X_FILTER_MUTE_CTRL, &val);
	ucontrol->value.integer.value[0] = (val & 0xc); // 0x3 is for the other adc

	return 0;
}

static int pcm1865_put_capture_mute(struct snd_kcontrol *kcontrol,
			      struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);
	int val;
	snd_soc_component_read(component, PCM186X_FILTER_MUTE_CTRL, &val);
	val = (ucontrol->value.integer.value[0] ? (val | 0xc) : (val & 0xf3));
	snd_soc_component_write(component, PCM186X_FILTER_MUTE_CTRL, val);

	return 0;
}

static int pcm1865_setup_dsp_put(struct snd_kcontrol *kcontrol,
                              struct snd_ctl_elem_value *ucontrol)
{
        struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);
	int i, val, clkstat;

	dev_info(component->dev, "Loading DSP Coefficients to ADC.\n");

        // Check current clock status to see if we need to start the sclk.
        snd_soc_component_read(component, PCM186X_CLK_STATUS, &clkstat);

        // Put the codec in master mode to activate its clocks.
        if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x11);

	// Setting PAGE 1
	val = set_page_1(component);
	if (val != 0) return val;

	// Write the static configuration for DSP coefficients
	for (i = 0; i < ARRAY_SIZE(pcm1865_dsp_vals); i++) {
		if (i == 0 || i == 7) continue; // Do not program the DSP coefficients corresponding to playback
		write_coefficient(component, pcm186x_dsp_registers[i], pcm1865_dsp_vals[i] ? 0x10 : 0x00, 0x00, 0x00);
	}

	// Set page 0
	snd_soc_component_write(component, PCM186X_PAGE, PCM186X_PAGE_0);

	// switch off sclk
	if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x01);

        return 0;
}

static int pcm1865_setup_dsp_get(struct snd_kcontrol *kcontrol,
				struct snd_ctl_elem_value *ucontrol)
{
	return 0;
}

static int pcm1865_get_playback_volume(struct snd_kcontrol *kcontrol,
				struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);
	int i, ret, clkstat;
	uint32_t val;
	uint8_t d0, d1, d2;

	// Check current clock status to see if we need to start the sclk.
	snd_soc_component_read(component, PCM186X_CLK_STATUS, &clkstat);

	// Put the codec in master mode to activate its clocks.
	if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x11);

	// Setting PAGE 1
	ret = set_page_1(component);
	if (ret != 0) return ret;

	val = read_coefficient(component, pcm186x_dsp_registers[0]);

	d0 = ((val & 0xff0000) >> 16);
	d1 = ((val & 0xff00) >> 8);
	d2 = (val & 0xff);

	for (i=0; i<553; i++){
		if (pcm1865_dsp_coefficients[0][i] == d0
				&& pcm1865_dsp_coefficients[1][i] == d1
				&& pcm1865_dsp_coefficients[2][i] == d2){
			ucontrol->value.integer.value[0] = i - 225;
			break;
		}
	}

	// Set page 0
	snd_soc_component_write(component, PCM186X_PAGE, PCM186X_PAGE_0);

	// switch off sclk
	if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x01);

	return 0;
}

static int pcm1865_put_playback_volume(struct snd_kcontrol *kcontrol,
				struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);
	int val, clkstat;

	// Check current clock status to see if we need to start the sclk.
	snd_soc_component_read(component, PCM186X_CLK_STATUS, &clkstat);

	// Put the codec in master mode to activate its clocks.
	if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x11);

	// Setting PAGE 1
	val = set_page_1(component);
	if (val != 0) return val;

	val = ucontrol->value.integer.value[0] + 225;
	write_coefficient(component, pcm186x_dsp_registers[0],
			pcm1865_dsp_coefficients[0][val], pcm1865_dsp_coefficients[1][val], pcm1865_dsp_coefficients[2][val]);
	write_coefficient(component, pcm186x_dsp_registers[7],
			pcm1865_dsp_coefficients[0][val], pcm1865_dsp_coefficients[1][val], pcm1865_dsp_coefficients[2][val]);

	// Set page 0
	snd_soc_component_write(component, PCM186X_PAGE, PCM186X_PAGE_0);

	// Switch off the sclk
	if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x01);

	return 0;
}

static const char * const pcm1865_playback_enum_text[] = {
	"None",
	"VIN1",
	"VIN2",
	"VIN2 + VIN1",
	"VIN3",
	"VIN3 + VIN1",
	"VIN3 + VIN2",
	"VIN3 + VIN2 + VIN1",
	"VIN4",
	"VIN4 + VIN1",
	"VIN4 + VIN2",
	"VIN4 + VIN2 + VIN1",
	"VIN4 + VIN3",
	"VIN4 + VIN3 + VIN1",
	"VIN4 + VIN3 + VIN2",
	"VIN4 + VIN3 + VIN2 + VIN1",
};

static SOC_ENUM_SINGLE_EXT_DECL(pcm1865_playback_enum, pcm1865_playback_enum_text);

static int pcm1865_get_playback_input(struct snd_kcontrol *kcontrol,
				struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);
	int val, ret;

	ret = snd_soc_component_read(component, PCM186X_ADC1_INPUT_SEL_L, &val);
	if (ret < 0) return -1;

	ucontrol->value.enumerated.item[0] = val;

	return 0;
}

static int pcm1865_put_playback_input(struct snd_kcontrol *kcontrol,
				struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_component *component = snd_soc_kcontrol_component(kcontrol);

	snd_soc_component_write(component, PCM186X_ADC1_INPUT_SEL_L, ucontrol->value.enumerated.item[0]);
	snd_soc_component_write(component, PCM186X_ADC1_INPUT_SEL_R, ucontrol->value.enumerated.item[0]);

	return 0;
}


static const struct snd_kcontrol_new pcm1863_snd_controls[] = {
	SOC_DOUBLE_R_S_TLV("ADC Capture Volume", PCM186X_PGA_VAL_CH1_L,
			   PCM186X_PGA_VAL_CH1_R, 0, -24, 80, 7, 0,
			   pcm186x_pga_tlv),
};

static const struct snd_kcontrol_new pcm1865_snd_controls[] = {
	SOC_DOUBLE_R_S_TLV("ADC1 Capture Volume", PCM186X_PGA_VAL_CH1_L,
			   PCM186X_PGA_VAL_CH1_R, 0, -24, 80, 7, 0,
			   pcm186x_pga_tlv),
	SOC_DOUBLE_R_S_TLV("ADC2 Capture Volume", PCM186X_PGA_VAL_CH2_L,
			   PCM186X_PGA_VAL_CH2_R, 0, -24, 80, 7, 0,
			   pcm186x_pga_tlv),

	SOC_SINGLE_BOOL_EXT("Mic Mute", 0, pcm1865_get_capture_mute, pcm1865_put_capture_mute),

	SOC_SINGLE_BOOL_EXT("Setup DSP", 0, pcm1865_setup_dsp_get, pcm1865_setup_dsp_put),

	// Volumes around or below [200] are inaudible. 0dB is found at approximately [480]. Set this up to use a range of 0-255
	// with an offset of +225. This will give a volume scale of 0->255 while picking actual values of 225->480.
	// Technically we have available volume range to [553] = +18 dB gain, but we won't use that unless it is really needed.
	SOC_SINGLE_EXT("Line Playback Volume", 0, 0, 255, 0, pcm1865_get_playback_volume, pcm1865_put_playback_volume),

	// Note: For AMFM Radio, set Line Playback Source = 0x2
	SOC_ENUM_EXT("Line Playback Source", pcm1865_playback_enum, pcm1865_get_playback_input, pcm1865_put_playback_input),

};

static const unsigned int pcm186x_adc_input_channel_sel_value[] = {
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
	0x10, 0x20, 0x30
};

static const char * const pcm186x_adcl_input_channel_sel_text[] = {
	"No Select",
	"VINL1[SE]",					/* Default for ADC1L */
	"VINL2[SE]",					/* Default for ADC2L */
	"VINL2[SE] + VINL1[SE]",
	"VINL3[SE]",
	"VINL3[SE] + VINL1[SE]",
	"VINL3[SE] + VINL2[SE]",
	"VINL3[SE] + VINL2[SE] + VINL1[SE]",
	"VINL4[SE]",
	"VINL4[SE] + VINL1[SE]",
	"VINL4[SE] + VINL2[SE]",
	"VINL4[SE] + VINL2[SE] + VINL1[SE]",
	"VINL4[SE] + VINL3[SE]",
	"VINL4[SE] + VINL3[SE] + VINL1[SE]",
	"VINL4[SE] + VINL3[SE] + VINL2[SE]",
	"VINL4[SE] + VINL3[SE] + VINL2[SE] + VINL1[SE]",
	"{VIN1P, VIN1M}[DIFF]",
	"{VIN4P, VIN4M}[DIFF]",
	"{VIN1P, VIN1M}[DIFF] + {VIN4P, VIN4M}[DIFF]"
};

static const char * const pcm186x_adcr_input_channel_sel_text[] = {
	"No Select",
	"VINR1[SE]",					/* Default for ADC1R */
	"VINR2[SE]",					/* Default for ADC2R */
	"VINR2[SE] + VINR1[SE]",
	"VINR3[SE]",
	"VINR3[SE] + VINR1[SE]",
	"VINR3[SE] + VINR2[SE]",
	"VINR3[SE] + VINR2[SE] + VINR1[SE]",
	"VINR4[SE]",
	"VINR4[SE] + VINR1[SE]",
	"VINR4[SE] + VINR2[SE]",
	"VINR4[SE] + VINR2[SE] + VINR1[SE]",
	"VINR4[SE] + VINR3[SE]",
	"VINR4[SE] + VINR3[SE] + VINR1[SE]",
	"VINR4[SE] + VINR3[SE] + VINR2[SE]",
	"VINR4[SE] + VINR3[SE] + VINR2[SE] + VINR1[SE]",
	"{VIN2P, VIN2M}[DIFF]",
	"{VIN3P, VIN3M}[DIFF]",
	"{VIN2P, VIN2M}[DIFF] + {VIN3P, VIN3M}[DIFF]"
};

static const struct soc_enum pcm186x_adc_input_channel_sel[] = {
	SOC_VALUE_ENUM_SINGLE(PCM186X_ADC1_INPUT_SEL_L, 0,
			      PCM186X_ADC_INPUT_SEL_MASK,
			      ARRAY_SIZE(pcm186x_adcl_input_channel_sel_text),
			      pcm186x_adcl_input_channel_sel_text,
			      pcm186x_adc_input_channel_sel_value),
	SOC_VALUE_ENUM_SINGLE(PCM186X_ADC1_INPUT_SEL_R, 0,
			      PCM186X_ADC_INPUT_SEL_MASK,
			      ARRAY_SIZE(pcm186x_adcr_input_channel_sel_text),
			      pcm186x_adcr_input_channel_sel_text,
			      pcm186x_adc_input_channel_sel_value),
	SOC_VALUE_ENUM_SINGLE(PCM186X_ADC2_INPUT_SEL_L, 0,
			      PCM186X_ADC_INPUT_SEL_MASK,
			      ARRAY_SIZE(pcm186x_adcl_input_channel_sel_text),
			      pcm186x_adcl_input_channel_sel_text,
			      pcm186x_adc_input_channel_sel_value),
	SOC_VALUE_ENUM_SINGLE(PCM186X_ADC2_INPUT_SEL_R, 0,
			      PCM186X_ADC_INPUT_SEL_MASK,
			      ARRAY_SIZE(pcm186x_adcr_input_channel_sel_text),
			      pcm186x_adcr_input_channel_sel_text,
			      pcm186x_adc_input_channel_sel_value),
};

static const struct snd_kcontrol_new pcm186x_adc_mux_controls[] = {
	SOC_DAPM_ENUM("ADC1 Left Input", pcm186x_adc_input_channel_sel[0]),
	SOC_DAPM_ENUM("ADC1 Right Input", pcm186x_adc_input_channel_sel[1]),
	SOC_DAPM_ENUM("ADC2 Left Input", pcm186x_adc_input_channel_sel[2]),
	SOC_DAPM_ENUM("ADC2 Right Input", pcm186x_adc_input_channel_sel[3]),
};

static const struct snd_soc_dapm_widget pcm1863_dapm_widgets[] = {
	SND_SOC_DAPM_INPUT("VINL1"),
	SND_SOC_DAPM_INPUT("VINR1"),
	SND_SOC_DAPM_INPUT("VINL2"),
	SND_SOC_DAPM_INPUT("VINR2"),
	SND_SOC_DAPM_INPUT("VINL3"),
	SND_SOC_DAPM_INPUT("VINR3"),
	SND_SOC_DAPM_INPUT("VINL4"),
	SND_SOC_DAPM_INPUT("VINR4"),

	SND_SOC_DAPM_MUX("ADC Left Capture Source", SND_SOC_NOPM, 0, 0,
			 &pcm186x_adc_mux_controls[0]),
	SND_SOC_DAPM_MUX("ADC Right Capture Source", SND_SOC_NOPM, 0, 0,
			 &pcm186x_adc_mux_controls[1]),

	/*
	 * Put the codec into SLEEP mode when not in use, allowing the
	 * Energysense mechanism to operate.
	 */
	SND_SOC_DAPM_ADC("ADC", "HiFi Capture", PCM186X_POWER_CTRL, 1,  1),
};

static const struct snd_soc_dapm_widget pcm1865_dapm_widgets[] = {
	SND_SOC_DAPM_INPUT("VINL1"),
	SND_SOC_DAPM_INPUT("VINR1"),
	SND_SOC_DAPM_INPUT("VINL2"),
	SND_SOC_DAPM_INPUT("VINR2"),
	SND_SOC_DAPM_INPUT("VINL3"),
	SND_SOC_DAPM_INPUT("VINR3"),
	SND_SOC_DAPM_INPUT("VINL4"),
	SND_SOC_DAPM_INPUT("VINR4"),

	SND_SOC_DAPM_MUX("ADC1 Left Capture Source", SND_SOC_NOPM, 0, 0,
			 &pcm186x_adc_mux_controls[0]),
	SND_SOC_DAPM_MUX("ADC1 Right Capture Source", SND_SOC_NOPM, 0, 0,
			 &pcm186x_adc_mux_controls[1]),
	SND_SOC_DAPM_MUX("ADC2 Left Capture Source", SND_SOC_NOPM, 0, 0,
			 &pcm186x_adc_mux_controls[2]),
	SND_SOC_DAPM_MUX("ADC2 Right Capture Source", SND_SOC_NOPM, 0, 0,
			 &pcm186x_adc_mux_controls[3]),

	/*
	 * Put the codec into SLEEP mode when not in use, allowing the
	 * Energysense mechanism to operate.
	 */
//	SND_SOC_DAPM_ADC("ADC1", "HiFi Capture 1", PCM186X_POWER_CTRL, 1,  1),
//	SND_SOC_DAPM_ADC("ADC2", "HiFi Capture 2", PCM186X_POWER_CTRL, 1,  1),
};

static const struct snd_soc_dapm_route pcm1863_dapm_routes[] = {
	{ "ADC Left Capture Source", NULL, "VINL1" },
	{ "ADC Left Capture Source", NULL, "VINR1" },
	{ "ADC Left Capture Source", NULL, "VINL2" },
	{ "ADC Left Capture Source", NULL, "VINR2" },
	{ "ADC Left Capture Source", NULL, "VINL3" },
	{ "ADC Left Capture Source", NULL, "VINR3" },
	{ "ADC Left Capture Source", NULL, "VINL4" },
	{ "ADC Left Capture Source", NULL, "VINR4" },

	{ "ADC", NULL, "ADC Left Capture Source" },

	{ "ADC Right Capture Source", NULL, "VINL1" },
	{ "ADC Right Capture Source", NULL, "VINR1" },
	{ "ADC Right Capture Source", NULL, "VINL2" },
	{ "ADC Right Capture Source", NULL, "VINR2" },
	{ "ADC Right Capture Source", NULL, "VINL3" },
	{ "ADC Right Capture Source", NULL, "VINR3" },
	{ "ADC Right Capture Source", NULL, "VINL4" },
	{ "ADC Right Capture Source", NULL, "VINR4" },

	{ "ADC", NULL, "ADC Right Capture Source" },
};

static const struct snd_soc_dapm_route pcm1865_dapm_routes[] = {
	{ "ADC1 Left Capture Source", NULL, "VINL1" },
	{ "ADC1 Left Capture Source", NULL, "VINR1" },
	{ "ADC1 Left Capture Source", NULL, "VINL2" },
	{ "ADC1 Left Capture Source", NULL, "VINR2" },
	{ "ADC1 Left Capture Source", NULL, "VINL3" },
	{ "ADC1 Left Capture Source", NULL, "VINR3" },
	{ "ADC1 Left Capture Source", NULL, "VINL4" },
	{ "ADC1 Left Capture Source", NULL, "VINR4" },

	{ "ADC1", NULL, "ADC1 Left Capture Source" },

	{ "ADC1 Right Capture Source", NULL, "VINL1" },
	{ "ADC1 Right Capture Source", NULL, "VINR1" },
	{ "ADC1 Right Capture Source", NULL, "VINL2" },
	{ "ADC1 Right Capture Source", NULL, "VINR2" },
	{ "ADC1 Right Capture Source", NULL, "VINL3" },
	{ "ADC1 Right Capture Source", NULL, "VINR3" },
	{ "ADC1 Right Capture Source", NULL, "VINL4" },
	{ "ADC1 Right Capture Source", NULL, "VINR4" },

	{ "ADC1", NULL, "ADC1 Right Capture Source" },

	{ "ADC2 Left Capture Source", NULL, "VINL1" },
	{ "ADC2 Left Capture Source", NULL, "VINR1" },
	{ "ADC2 Left Capture Source", NULL, "VINL2" },
	{ "ADC2 Left Capture Source", NULL, "VINR2" },
	{ "ADC2 Left Capture Source", NULL, "VINL3" },
	{ "ADC2 Left Capture Source", NULL, "VINR3" },
	{ "ADC2 Left Capture Source", NULL, "VINL4" },
	{ "ADC2 Left Capture Source", NULL, "VINR4" },

	{ "ADC2", NULL, "ADC2 Left Capture Source" },

	{ "ADC2 Right Capture Source", NULL, "VINL1" },
	{ "ADC2 Right Capture Source", NULL, "VINR1" },
	{ "ADC2 Right Capture Source", NULL, "VINL2" },
	{ "ADC2 Right Capture Source", NULL, "VINR2" },
	{ "ADC2 Right Capture Source", NULL, "VINL3" },
	{ "ADC2 Right Capture Source", NULL, "VINR3" },
	{ "ADC2 Right Capture Source", NULL, "VINL4" },
	{ "ADC2 Right Capture Source", NULL, "VINR4" },

	{ "ADC2", NULL, "ADC2 Right Capture Source" },
};

static int pcm186x_hw_params(struct snd_pcm_substream *substream,
			     struct snd_pcm_hw_params *params,
			     struct snd_soc_dai *dai)
{
	struct snd_soc_component *component = dai->component;
	struct pcm186x_priv *priv = snd_soc_component_get_drvdata(component);
	unsigned int rate = params_rate(params);
	snd_pcm_format_t format = params_format(params);
	unsigned int width = params_width(params);
	unsigned int channels = params_channels(params);
	unsigned int div_lrck;
	unsigned int div_bck;
	u8 tdm_tx_sel = 0;
	u8 pcm_cfg = 0;

	dev_dbg(component->dev, "%s() rate=%u format=0x%x width=%u channels=%u\n",
		__func__, rate, format, width, channels);

	switch (width) {
	case 16:
		pcm_cfg = PCM186X_PCM_CFG_RX_WLEN_16 <<
			  PCM186X_PCM_CFG_RX_WLEN_SHIFT |
			  PCM186X_PCM_CFG_TX_WLEN_16 <<
			  PCM186X_PCM_CFG_TX_WLEN_SHIFT;
		break;
	case 20:
		pcm_cfg = PCM186X_PCM_CFG_RX_WLEN_20 <<
			  PCM186X_PCM_CFG_RX_WLEN_SHIFT |
			  PCM186X_PCM_CFG_TX_WLEN_20 <<
			  PCM186X_PCM_CFG_TX_WLEN_SHIFT;
		break;
	case 24:
		pcm_cfg = PCM186X_PCM_CFG_RX_WLEN_24 <<
			  PCM186X_PCM_CFG_RX_WLEN_SHIFT |
			  PCM186X_PCM_CFG_TX_WLEN_24 <<
			  PCM186X_PCM_CFG_TX_WLEN_SHIFT;
		break;
	case 32:
		pcm_cfg = PCM186X_PCM_CFG_RX_WLEN_32 <<
			  PCM186X_PCM_CFG_RX_WLEN_SHIFT |
			  PCM186X_PCM_CFG_TX_WLEN_32 <<
			  PCM186X_PCM_CFG_TX_WLEN_SHIFT;
		break;
	default:
		return -EINVAL;
	}

	snd_soc_component_update_bits(component, PCM186X_PCM_CFG,
			    PCM186X_PCM_CFG_RX_WLEN_MASK |
			    PCM186X_PCM_CFG_TX_WLEN_MASK,
			    pcm_cfg);

	div_lrck = width * channels;

	if (priv->is_tdm_mode) {
		/* Select TDM transmission data */
		switch (channels) {
		case 2:
			tdm_tx_sel = PCM186X_TDM_TX_SEL_2CH;
			break;
		case 4:
			tdm_tx_sel = PCM186X_TDM_TX_SEL_4CH;
			break;
		case 6:
			tdm_tx_sel = PCM186X_TDM_TX_SEL_6CH;
			break;
		default:
			return -EINVAL;
		}

		snd_soc_component_update_bits(component, PCM186X_TDM_TX_SEL,
				    PCM186X_TDM_TX_SEL_MASK, tdm_tx_sel);

		/* In DSP/TDM mode, the LRCLK divider must be 256 */
		div_lrck = 256;

		/* Configure 1/256 duty cycle for LRCK */
		snd_soc_component_update_bits(component, PCM186X_PCM_CFG,
				    PCM186X_PCM_CFG_TDM_LRCK_MODE,
				    PCM186X_PCM_CFG_TDM_LRCK_MODE);
	}

	/* Only configure clock dividers in master mode. */
	if (priv->is_master_mode) {
		div_bck = priv->sysclk / (div_lrck * rate);

		dev_dbg(component->dev,
			"%s() master_clk=%u div_bck=%u div_lrck=%u\n",
			__func__, priv->sysclk, div_bck, div_lrck);

		snd_soc_component_write(component, PCM186X_BCK_DIV, div_bck - 1);
		snd_soc_component_write(component, PCM186X_LRK_DIV, div_lrck - 1);
	}

	return 0;
}

static int pcm186x_set_fmt(struct snd_soc_dai *dai, unsigned int format)
{
	struct snd_soc_component *component = dai->component;
	struct pcm186x_priv *priv = snd_soc_component_get_drvdata(component);
	u8 clk_ctrl = 0;
	u8 pcm_cfg = 0;

	dev_dbg(component->dev, "%s() format=0x%x\n", __func__, format);

	/* set master/slave audio interface */
	switch (format & SND_SOC_DAIFMT_MASTER_MASK) {
	case SND_SOC_DAIFMT_CBM_CFM:
		if (!priv->sysclk) {
			dev_err(component->dev, "operating in master mode requires sysclock to be configured\n");
			return -EINVAL;
		}
		clk_ctrl |= PCM186X_CLK_CTRL_MST_MODE;
		priv->is_master_mode = true;
		break;
	case SND_SOC_DAIFMT_CBS_CFS:
		priv->is_master_mode = false;
		break;
	default:
		dev_err(component->dev, "Invalid DAI master/slave interface\n");
		return -EINVAL;
	}

	/* set interface polarity */
	switch (format & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
		break;
	default:
		dev_err(component->dev, "Inverted DAI clocks not supported\n");
		return -EINVAL;
	}

	/* set interface format */
	switch (format & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_I2S:
		pcm_cfg = PCM186X_PCM_CFG_FMT_I2S;
		break;
	case SND_SOC_DAIFMT_LEFT_J:
		pcm_cfg = PCM186X_PCM_CFG_FMT_LEFTJ;
		break;
	case SND_SOC_DAIFMT_DSP_A:
		priv->tdm_offset += 1;
		/* fall through */
		/* DSP_A uses the same basic config as DSP_B
		 * except we need to shift the TDM output by one BCK cycle
		 */
	case SND_SOC_DAIFMT_DSP_B:
		priv->is_tdm_mode = true;
		pcm_cfg = PCM186X_PCM_CFG_FMT_TDM;
		break;
	default:
		dev_err(component->dev, "Invalid DAI format\n");
		return -EINVAL;
	}

	snd_soc_component_update_bits(component, PCM186X_CLK_CTRL,
			    PCM186X_CLK_CTRL_MST_MODE, clk_ctrl);

	snd_soc_component_write(component, PCM186X_TDM_TX_OFFSET, priv->tdm_offset);

	snd_soc_component_update_bits(component, PCM186X_PCM_CFG,
			    PCM186X_PCM_CFG_FMT_MASK, pcm_cfg);

	return 0;
}

static int pcm186x_set_tdm_slot(struct snd_soc_dai *dai, unsigned int tx_mask,
				unsigned int rx_mask, int slots, int slot_width)
{
	struct snd_soc_component *component = dai->component;
	struct pcm186x_priv *priv = snd_soc_component_get_drvdata(component);
	unsigned int first_slot, last_slot, tdm_offset;

	dev_dbg(component->dev,
		"%s() tx_mask=0x%x rx_mask=0x%x slots=%d slot_width=%d\n",
		__func__, tx_mask, rx_mask, slots, slot_width);

	if (!tx_mask) {
		dev_err(component->dev, "tdm tx mask must not be 0\n");
		return -EINVAL;
	}

	first_slot = __ffs(tx_mask);
	last_slot = __fls(tx_mask);

	if (last_slot - first_slot != hweight32(tx_mask) - 1) {
		dev_err(component->dev, "tdm tx mask must be contiguous\n");
		return -EINVAL;
	}

	tdm_offset = first_slot * slot_width;

	if (tdm_offset > 255) {
		dev_err(component->dev, "tdm tx slot selection out of bounds\n");
		return -EINVAL;
	}

	priv->tdm_offset = tdm_offset;

	return 0;
}

static int pcm186x_set_dai_sysclk(struct snd_soc_dai *dai, int clk_id,
				  unsigned int freq, int dir)
{
	struct snd_soc_component *component = dai->component;
	struct pcm186x_priv *priv = snd_soc_component_get_drvdata(component);

	dev_dbg(component->dev, "%s() clk_id=%d freq=%u dir=%d\n",
		__func__, clk_id, freq, dir);

	priv->sysclk = freq;

	return 0;
}

static const struct snd_soc_dai_ops pcm186x_dai_ops = {
	.set_sysclk = pcm186x_set_dai_sysclk,
	.set_tdm_slot = pcm186x_set_tdm_slot,
	.set_fmt = pcm186x_set_fmt,
	.hw_params = pcm186x_hw_params,
};

static struct snd_soc_dai_driver pcm1863_dai = {
	.name = "pcm1863-aif",
	.capture = {
		 .stream_name = "Capture",
		 .channels_min = 1,
		 .channels_max = 2,
		 .rates = PCM186X_RATES,
		 .formats = PCM186X_FORMATS,
	 },
	.ops = &pcm186x_dai_ops,
};

static struct snd_soc_dai_driver pcm1865_dai = {
	.name = "pcm1865-aif",
	.capture = {
		 .stream_name = "Capture",
		 .channels_min = 1,
		 .channels_max = 4,
		 .rates = PCM186X_RATES,
		 .formats = PCM186X_FORMATS,
	 },
	.ops = &pcm186x_dai_ops,
};

static int pcm186x_power_on(struct snd_soc_component *component)
{
	struct pcm186x_priv *priv = snd_soc_component_get_drvdata(component);
	int ret = 0, i, val, clkstat;

	ret = regulator_bulk_enable(ARRAY_SIZE(priv->supplies),
				    priv->supplies);
	if (ret)
		return ret;

	regcache_cache_only(priv->regmap, false);
	ret = regcache_sync(priv->regmap);
	if (ret) {
		dev_err(component->dev, "Failed to restore cache\n");
		regcache_cache_only(priv->regmap, true);
		regulator_bulk_disable(ARRAY_SIZE(priv->supplies),
				       priv->supplies);
		return ret;
	}

	snd_soc_component_update_bits(component, PCM186X_POWER_CTRL,
			    PCM186X_PWR_CTRL_PWRDN, 0);

	{
		// Check current clock status to see if we need to start the sclk.
		snd_soc_component_read(component, PCM186X_CLK_STATUS, &clkstat);

		// Put the codec in master mode to activate its clocks.
		if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x11);

		snd_soc_component_write(component, PCM186X_PAGE, PCM186X_PAGE_0);

		// ADC1 mixes with I2S-IN and is fed out to DACs.
		// ADC2 is fed back to the SBC.
		// Must attach AIN1 (MIC) to ADC2
		// Must attach AIN2 (AMFM) to ADC1
		//
		// Set up ADC default inputs. ADC1 --> AIN2, ADC2 --> AIN1
		// Reg 0x08 Val 0x02
		// Reg 0x09 Val 0x02
		// Note that input selection can be set later using MIXER. This just
		// establishes out default configuration for expected use case.
		snd_soc_component_write(component, PCM186X_ADC2_INPUT_SEL_L, 0x01);
		snd_soc_component_write(component, PCM186X_ADC2_INPUT_SEL_R, 0x01);

		// Set ADC default to 16 bit sample length
		snd_soc_component_write(component, PCM186X_PCM_CFG, 0xcc);

		// Set GPIO0/1 for I2S
		// Reg 0x10 Val 0x65
		snd_soc_component_write(component, PCM186X_GPIO1_0_CTRL, 0x65);

		dev_info(component->dev, "Loading DSP Coefficients to ADC.\n");

		// Setting PAGE 1
		val = set_page_1(component);
		if (val != 0) return val;

		// Write the static configuration for DSP coefficients
		for (i = 0; i < ARRAY_SIZE(pcm1865_dsp_vals); i++) {
			if (i == 0 || i == 7) continue; // Do not program the DSP coefficients corresponding to playback
			write_coefficient(component, pcm186x_dsp_registers[i], pcm1865_dsp_vals[i] ? 0x10 : 0x00, 0x00, 0x00);
		}

		// Set page 0
		snd_soc_component_write(component, PCM186X_PAGE, PCM186X_PAGE_0);

                // Put the codec out of master mode to deactivate its clocks.
                if (clkstat != 0) snd_soc_component_write(component, PCM186X_CLK_CTRL, 0x01);
	}

	return 0;
}

static int pcm186x_power_off(struct snd_soc_component *component)
{
	struct pcm186x_priv *priv = snd_soc_component_get_drvdata(component);
	int ret;

	snd_soc_component_update_bits(component, PCM186X_POWER_CTRL,
			    PCM186X_PWR_CTRL_PWRDN, PCM186X_PWR_CTRL_PWRDN);

	regcache_cache_only(priv->regmap, true);

	ret = regulator_bulk_disable(ARRAY_SIZE(priv->supplies),
				     priv->supplies);
	if (ret)
		return ret;

	return 0;
}

static int pcm186x_set_bias_level(struct snd_soc_component *component,
				  enum snd_soc_bias_level level)
{
	dev_dbg(component->dev, "## %s: %d -> %d\n", __func__,
		snd_soc_component_get_bias_level(component), level);

	switch (level) {
	case SND_SOC_BIAS_ON:
		break;
	case SND_SOC_BIAS_PREPARE:
		break;
	case SND_SOC_BIAS_STANDBY:
		if (snd_soc_component_get_bias_level(component) == SND_SOC_BIAS_OFF)
			pcm186x_power_on(component);
		break;
	case SND_SOC_BIAS_OFF:
		pcm186x_power_off(component);
		break;
	}

	return 0;
}

static struct snd_soc_component_driver soc_codec_dev_pcm1863 = {
	.set_bias_level		= pcm186x_set_bias_level,
	.controls		= pcm1863_snd_controls,
	.num_controls		= ARRAY_SIZE(pcm1863_snd_controls),
	.dapm_widgets		= pcm1863_dapm_widgets,
	.num_dapm_widgets	= ARRAY_SIZE(pcm1863_dapm_widgets),
	.dapm_routes		= pcm1863_dapm_routes,
	.num_dapm_routes	= ARRAY_SIZE(pcm1863_dapm_routes),
	.idle_bias_on		= 1,
	.use_pmdown_time	= 1,
	.endianness		= 1,
	.non_legacy_dai_naming	= 1,
};

static struct snd_soc_component_driver soc_codec_dev_pcm1865 = {
	.set_bias_level		= pcm186x_set_bias_level,
	.controls		= pcm1865_snd_controls,
	.num_controls		= ARRAY_SIZE(pcm1865_snd_controls),
	.dapm_widgets		= pcm1865_dapm_widgets,
	.num_dapm_widgets	= ARRAY_SIZE(pcm1865_dapm_widgets),
//	.dapm_routes		= pcm1865_dapm_routes,
//	.num_dapm_routes	= ARRAY_SIZE(pcm1865_dapm_routes),
	.suspend_bias_off	= 1,
	.idle_bias_on		= 1,
	.use_pmdown_time	= 1,
	.endianness		= 1,
	.non_legacy_dai_naming	= 1,
};

static bool pcm186x_volatile(struct device *dev, unsigned int reg)
{
	switch (reg) {
	case PCM186X_PAGE:
	case PCM186X_DEVICE_STATUS:
	case PCM186X_FSAMPLE_STATUS:
	case PCM186X_DIV_STATUS:
	case PCM186X_CLK_STATUS:
	case PCM186X_SUPPLY_STATUS:
	case PCM186X_MMAP_STAT_CTRL:
	case PCM186X_MMAP_ADDRESS:
	case PCM186X_MEM_RDATA0:
	case PCM186X_MEM_RDATA1:
	case PCM186X_MEM_RDATA2:
		return true;
	}

	return false;
}

static const struct regmap_range_cfg pcm186x_range = {
	.name = "Pages",
	.range_max = PCM186X_MAX_REGISTER,
	.selector_reg = PCM186X_PAGE,
	.selector_mask = 0xff,
	.window_len = PCM186X_PAGE_LEN,
};

const struct regmap_config pcm186x_regmap = {
	.reg_bits = 8,
	.val_bits = 8,

	.volatile_reg = pcm186x_volatile,

	.ranges = &pcm186x_range,
	.num_ranges = 1,

	.max_register = PCM186X_MAX_REGISTER,

	.cache_type = REGCACHE_RBTREE,
};
EXPORT_SYMBOL_GPL(pcm186x_regmap);

int pcm186x_probe(struct device *dev, enum pcm186x_type type, int irq,
		  struct regmap *regmap)
{
	struct pcm186x_priv *priv;
	int i, ret;

	priv = devm_kzalloc(dev, sizeof(struct pcm186x_priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	dev_set_drvdata(dev, priv);
	priv->regmap = regmap;

	for (i = 0; i < ARRAY_SIZE(priv->supplies); i++)
		priv->supplies[i].supply = pcm186x_supply_names[i];

	ret = devm_regulator_bulk_get(dev, ARRAY_SIZE(priv->supplies),
				      priv->supplies);
	if (ret) {
		dev_err(dev, "failed to request supplies: %d\n", ret);
		return ret;
	}

	ret = regulator_bulk_enable(ARRAY_SIZE(priv->supplies),
				    priv->supplies);
	if (ret) {
		dev_err(dev, "failed enable supplies: %d\n", ret);
		return ret;
	}

	/* Reset device registers for a consistent power-on like state */
	ret = regmap_write(regmap, PCM186X_PAGE, PCM186X_RESET);
	if (ret) {
		dev_err(dev, "failed to write device: %d\n", ret);
		return ret;
	}

	ret = regulator_bulk_disable(ARRAY_SIZE(priv->supplies),
				     priv->supplies);
	if (ret) {
		dev_err(dev, "failed disable supplies: %d\n", ret);
		return ret;
	}

	switch (type) {
	case PCM1865:
	case PCM1864:
		ret = devm_snd_soc_register_component(dev, &soc_codec_dev_pcm1865,
					     &pcm1865_dai, 1);
		break;
	case PCM1863:
	case PCM1862:
	default:
		ret = devm_snd_soc_register_component(dev, &soc_codec_dev_pcm1863,
					     &pcm1863_dai, 1);
	}
	if (ret) {
		dev_err(dev, "failed to register CODEC: %d\n", ret);
		return ret;
	}

	return 0;
}
EXPORT_SYMBOL_GPL(pcm186x_probe);

MODULE_AUTHOR("Andreas Dannenberg <dannenberg@ti.com>");
MODULE_AUTHOR("Andrew F. Davis <afd@ti.com>");
MODULE_DESCRIPTION("PCM186x Universal Audio ADC driver");
MODULE_LICENSE("GPL v2");
